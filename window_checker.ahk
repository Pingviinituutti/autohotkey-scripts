#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
; #Warn  ; Enable warnings to assist with detecting common errors.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.

; Only use the qverty bindings for certain applications
#IfWinActive, Cities: Skylines
  #Include dvorak_to_qwerty.ahk
  Return
#IfWinActive, Stranded Deep
  #Include dvorak_to_qwerty.ahk
  Return
#IfWinActive
; ctrl+alt+a to add the current window to list of windows that should map dvorak to qwerty
  ^!a::
  addNewWindow() {
    WinGetTitle, Title, A
    window_string := "#IfWinActive, " Title "`n`t#Include dvorak_to_qwerty.ahk`n`tReturn`n"
    ; Loop, Read, %A_ScriptName%, tmp.ahk
    Loop, Read, %A_ScriptName%, tmp.ahk
      {
        CurrLine := A_LoopReadLine
        If (InStr(CurrLine, "#IfWinActive") and Not InStr(CurrLine, ","))
            FileAppend, %window_string%
        FileAppend, %CurrLine%`n
      }
    FileCopy, tmp.ahk, %A_ScriptName%, 1
    FileDelete, tmp.ahk
    MsgBox '%Title%' added to window list.
    Reload
  }
  Return
