﻿[::-
]::=

'::q
,::w
.::e
p::r
y::t
f::y
g::u
c::i
r::o
l::p
/::å
=::]
;\::\ ;no change

;a::a ;no change
o::s
e::d
u::f
i::g
d::h
h::j
t::k
n::l
s::ö
-::ä

SC027::z
q::x
j::c
k::v
x::b
b::n
;m::m ;no change
w::,
v::.
z::-
